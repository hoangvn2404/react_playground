@Team = React.createClass
	getInitialState: ->
		point: 0

	plus: ->
		@setState point: @state.point + 1

	minus: ->
		if @state.point > 0
			@setState point: @state.point - 1

	render: ->
    div className: 'col-md-4',
      div className: 'panel panel-primary',
      	div className: 'panel-heading', style: { textAlign: 'center' },
      		h1 {}, @props.team.name
      	div 
      		className: 'outer panel-body', 
      		style:{width: '100%', height: 300, paddingTop:100, backgroundImage: "url(" + @props.team.photo.url + ")", backgroundSize:'cover'},
      		div className:'inner',
      			h1 style: {color: 'white', fontSize:100},
      				@state.point
      	div className: 'panel-footer', style: {overflow:'hidden'},
      		button className:'btn btn-info pull-right', onClick: @plus, 'Plus'
      		button className:'btn btn-danger pull-left', onClick: @minus, 'Minus'
