class Chapter < ApplicationRecord
  belongs_to :manga
  has_many :images

  def update_images

    chapter_info = Nokogiri::HTML(open(url))
    max_page = chapter_info.css('.tbtitle.dropdown_parent.dropdown_right.mmh .text').text().delete(" ⤵").to_i

    for number in 1..max_page do
      image_link = url + "page/" + number.to_s
      image_url = Nokogiri::HTML(open(image_link)).css('.open').first.attr('src')
      self.images.create(index: number, url: image_url)
    end

  end
end
