class Manga < ApplicationRecord
  has_many :chapters

  def update_info
    manga_info = Nokogiri::HTML(open(url))
    cover = manga_info.css('.thumbnail img').first.attr('src')
    title = manga_info.xpath("//div[@class='large comic']/h1/text()").to_s.squish
    description = manga_info.xpath("//b[text()='Synopsis']/following-sibling::text()[1]").text
    self.update(cover: cover, title: title, description: description)

    manga_info.css('.element .title a').each do |f, index|
      chapter = self.chapters.find_or_create_by(url: f.attr('href'), title: f.text)
      chapter.update(index: index)
    end

  end
end


# t.string   "title"
# t.string   "description"
# t.string   "cover"
# t.string   "thumbnail"