class Product < ApplicationRecord

  def self.to_csv products
    attributes = %w{code name price}
    (CSV.generate(headers: true) do |csv|
      csv << attributes
      products.each do |p|
        csv << attributes.map{ |attr| p.send(attr) }
      end
    end).encode("Windows-1252", :undef => :replace, :replace => '')
  end


  def self.import(file)
    xlsx = open_spreadsheet(file)
    (2..xlsx.last_row).each do |i|
      data  = xlsx.row(i)
      Product.create(code: data[1])
    end
  end

  def self.open_spreadsheet(file)
    Roo::Spreadsheet.open(file.path)
  end  
end
