json.extract! manga, :id, :title, :description, :source, :created_at, :updated_at
json.url manga_url(manga, format: :json)