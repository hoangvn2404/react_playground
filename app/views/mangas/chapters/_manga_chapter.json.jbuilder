json.extract! manga_chapter, :id, :chapter_name, :chapter_index, :manga_id, :uploaded_date, :created_at, :updated_at
json.url manga_chapter_url(manga_chapter, format: :json)