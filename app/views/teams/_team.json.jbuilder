json.extract! team, :id, :name, :photo, :point, :created_at, :updated_at
json.url team_url(team, format: :json)