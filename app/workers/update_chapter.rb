class UpdateChapter
  include Sidekiq::Worker

  def perform(chapter_id)
    chapter = Chapter.find(chapter_id)
    chapter.update_images
  end

end