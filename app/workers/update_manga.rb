class UpdateManga
  include Sidekiq::Worker
  def perform(manga_id)
    manga = Manga.find(manga_id)
    manga.update_info
    
  end
end