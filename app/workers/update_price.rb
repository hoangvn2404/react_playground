class UpdatePrice
  include Sidekiq::Worker
  def perform()
    Product.all.each do |p|
      begin
        link = "http://www.lazada.vn/catalog/?q=" + p.code
        product = Nokogiri::HTML(open(link))
        if product.css(".product-card__description").blank?
          p.update(name: "product not found")
        else
          name = product.css(".product-card__description .product-card__name-wrap span").attr('title').value
          price = product.css(".product-card__description div.price-block--grid .product-card__price").text()
          p.update(name: name, price: price)
        end
      rescue
        p.update(name: "product code error")
      end
    end
  end
end