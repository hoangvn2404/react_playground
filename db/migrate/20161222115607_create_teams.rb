class CreateTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
      t.string :name
      t.string :photo
      t.integer :point, default: 0

      t.timestamps
    end
  end
end
