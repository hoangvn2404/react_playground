class CreateChapters < ActiveRecord::Migration[5.0]
  def change
    create_table :chapters do |t|
      t.string :title
      t.integer :index
      t.integer :manga_id
      t.date :uploaded_date
      t.string :url

      t.timestamps
    end
  end
end
