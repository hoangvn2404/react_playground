class CreateImages < ActiveRecord::Migration[5.0]
  def change
    create_table :images do |t|
      t.integer :index
      t.string :url
      t.integer :chapter_id

      t.timestamps
    end
  end
end
