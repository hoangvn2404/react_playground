require 'nokogiri'
require 'open-uri'

namespace :manga do
  task update_yomanga_list: :environment do
    manga_directory = Nokogiri::HTML(open('https://yomanga.co/reader/directory/2'))
    manga_directory.xpath("//div[@class='group']/div[@class='title']/a").each do |f|
      Manga.find_or_create_by(url: f.attr('href'), source: 'yomanga.co')
    end
  end

  task update_mangas: :environment do
    Manga.all.each do |manga|
      # UpdateManga.perform_async(manga.id)
      manga.update_info
      puts "manga: #{manga.title}"
      manga.chapters.each do |chapter|
        chapter.update_images
        puts "   chapter: #{chapter.title}"
      end
    end
  end

  task update_chapters: :environment do
    Chapter.pluck(:id).each { |i| UpdateChapter(i) }
  end
end