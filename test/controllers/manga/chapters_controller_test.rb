require 'test_helper'

class Manga::ChaptersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @manga_chapter = manga_chapters(:one)
  end

  test "should get index" do
    get manga_chapters_url
    assert_response :success
  end

  test "should get new" do
    get new_manga_chapter_url
    assert_response :success
  end

  test "should create manga_chapter" do
    assert_difference('Manga::Chapter.count') do
      post manga_chapters_url, params: { manga_chapter: { chapter_index: @manga_chapter.chapter_index, chapter_name: @manga_chapter.chapter_name, manga_id: @manga_chapter.manga_id, uploaded_date: @manga_chapter.uploaded_date } }
    end

    assert_redirected_to manga_chapter_url(Manga::Chapter.last)
  end

  test "should show manga_chapter" do
    get manga_chapter_url(@manga_chapter)
    assert_response :success
  end

  test "should get edit" do
    get edit_manga_chapter_url(@manga_chapter)
    assert_response :success
  end

  test "should update manga_chapter" do
    patch manga_chapter_url(@manga_chapter), params: { manga_chapter: { chapter_index: @manga_chapter.chapter_index, chapter_name: @manga_chapter.chapter_name, manga_id: @manga_chapter.manga_id, uploaded_date: @manga_chapter.uploaded_date } }
    assert_redirected_to manga_chapter_url(@manga_chapter)
  end

  test "should destroy manga_chapter" do
    assert_difference('Manga::Chapter.count', -1) do
      delete manga_chapter_url(@manga_chapter)
    end

    assert_redirected_to manga_chapters_url
  end
end
